from flask import Flask
from flask import request, render_template
import random

app = Flask(__name__)

@app.route(rule='/hello', methods=["GET"]) #@ dekorator, rule - ścieżka, metoda GET
def hello_world():
    app.logger.warning("hello world") #metoda, która wyświetla inf o zdarzeniach - skrypt o pożarach
    app.logger.info("hello world2")
    if "name" in request.args:
        return "Witaj " + request.args["name"] 
    else:
        return "Witaj nieznajomy"

@app.route(rule='/hello', methods=["POST"])
def hello_world2():
    return "Brawo, udało się wysłać requesta metodą POST"

@app.route(rule="/koala-penguins", methods=["GET"]) #odpalamy http://localhost:5000/koala-pengiuns
def orzel_reszka():
    rand_int = random.randint(1,11)
    if rand_int >=6:
        return render_template("koala.html")
    else:
        return render_template("penguins.html")

@app.route(rule="/pet", methods=["POST"])
def add_pet():
    print(request.get_json())
    return "OK", 200 #status code 400 - bad request, 200 - created

@app.route(rule="/", methods=["GET"])
def index():
    return render_template("index.html") #odwołanie się do pliku html

app.run(debug=True, port=5000)  #http://127.0.0.1:5000/ lub localhost:5000